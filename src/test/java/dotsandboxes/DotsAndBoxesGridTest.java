package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testBoxComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw the lines around a box at (1, 1)
        grid.drawHorizontal(1, 1, 1);
        grid.drawHorizontal(1, 2, 1);
        grid.drawVertical(1, 1, 1);
        grid.drawVertical(2, 1, 1);

        assertTrue(grid.boxComplete(1, 1), "Box should be complete");
    }

    @Test
    public void testBoxIncomplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw three lines around a box at (1, 1)
        grid.drawHorizontal(1, 1, 1);
        grid.drawHorizontal(1, 2, 1);
        grid.drawVertical(1, 1, 1);

        assertFalse(grid.boxComplete(1, 1), "Box should not be complete");
    }

    @Test
    public void testDrawHorizontalLineTwice() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        grid.drawHorizontal(1, 1, 1);
        
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(1, 1, 1);
        });

        assertEquals("Horizontal line at (1, 1) is already drawn", exception.getMessage());
    }

    @Test
    public void testDrawVerticalLineTwice() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        grid.drawVertical(1, 1, 1);
        
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(1, 1, 1);
        });

        assertEquals("Vertical line at (1, 1) is already drawn", exception.getMessage());
    }
}